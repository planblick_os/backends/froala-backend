FROM python:3.9-slim

ENV PYTHONUNBUFFERED 1

RUN apt-get update && apt-get install libmagickwand-dev -y && apt-get clean
RUN pip install --upgrade pip

# install pip modules (own filesystem layer)
COPY src/requirements.txt /src/requirements.txt
RUN pip3 install -r /src/requirements.txt --no-cache-dir
RUN pip3 install --upgrade gevent

COPY src /src

RUN chmod 755 /src/app && chmod 755 /src/runApp.sh && chmod +x /src/runApp.sh

STOPSIGNAL SIGINT

CMD ["/src/runApp.sh"]

