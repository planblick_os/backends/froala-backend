from main import app
import cherrypy
import os
from inspect import getframeinfo, stack

from autorun import Autorun

if __name__ == '__main__':
    Autorun().post_deployment()

    class Static(object): pass


    # Mount the application
    cherrypy.tree.graft(app, "/")

    # Add static file serving to enable swagger GUI
    caller = getframeinfo(stack()[0][0])
    PATH = os.path.abspath(os.path.dirname(__file__))
    cherrypy.tree.mount(Static(), "/public", config={
        '/': {
            'tools.staticdir.on': True,
            'tools.staticdir.dir': PATH + "/public",
            'tools.staticdir.index': 'index.html'
        }
    })
    # Unsubscribe the default server
    cherrypy.server.unsubscribe()

    # Instantiate a new server object
    server = cherrypy._cpserver.Server()

    # Configure the server object
    server.socket_host = "0.0.0.0"
    server.socket_port = 8000
    server.thread_pool = 30

    # Subscribe this server
    server.subscribe()

    # Start the server engine (Option 1 *and* 2)
    cherrypy.engine.start()
    cherrypy.engine.block()
